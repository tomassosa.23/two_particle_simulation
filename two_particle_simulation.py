import numpy as np 
import matplotlib.pyplot as plt 
from dataclasses import dataclass
from scipy.integrate import odeint

@dataclass
class Particulas:
    c: float
    m: float 

class Interaccion:
    """
    m1: masa particula 1
    m2: masa particula 2
    c1: carga particula 1
    c2: carga particula 2
    v1: lista ordenada en x,y,z de las componentes de la velocidad de la partícula 1
    v2: lista ordenada en x,y,z de las componentes de la velocidad de la partícula 2
    p1: lista ordenada en x,y,z de las componentes de la posicion de la partícula 1
    p2: lista ordenada en x,y,z de las componentes de la posicion de la partícula 2
    """
    #Defino un par de particulas con sus propiedades para poder definir una función que caracterize la interaccion
    
    
    def __init__(self, c1, c2, m1, m2, p1, p2, v1, v2):
        self._c1=c1 # Carga particula 1
        self._c2=c2 # Carga particula 2

        self._m1=m1 # Masa particula 1
        self._m2=m2 # Masa particula 2

        self._p1=p1 # Posicion inicial particula 1
        self._p2=p2 # Posicion inicial particula 2

        self._v1=v1 # Velocidad particula 1
        self._v2=v2 # Velocidad particula 2

    def Lorentz_force(self, c, v, B):
        # Fuerza de Lorentz
        F = c*np.cross(v, B)
        return F
    
    def Coulomb_force(self, p1, p2):
        
        #epsilon_0 = 1.
        #K = 1 / (4*np.pi*epsilon_0)
        #K = 8.9875517873681764e9  # Coulomb's constant
        K=1.
        # Fuerza de Coulomb
        r_square = ((p1[1]-p2[1])**2+(p1[0]-p2[0])**2+(p1[2]-p2[2])**2)**3/2
        assert r_square > 0.1, 'La distancia entre las partículas no puede ser cero'

        F = K*self._c1*self._c2*(np.array(p1)-np.array(p2))/r_square
        return F
    
    def acceleration(self, p1, p2, v1, v2, B):

        # Cálculo de la aceleración de la partícula 1
        ax1 = (self.Lorentz_force(self._c1, v1, B)[0] + self.Coulomb_force(p1, p2)[0])/self._m1
        ay1 = (self.Lorentz_force(self._c1, v1, B)[1] + self.Coulomb_force(p1, p2)[1])/self._m1
        az1 = (self.Lorentz_force(self._c1, v1, B)[2] + self.Coulomb_force(p1, p2)[2])/self._m1

        # Cálculo de la aceleración de la partícula 2
        ax2 = (self.Lorentz_force(self._c2, v2, B)[0] - self.Coulomb_force(p1, p2)[0])/self._m2
        ay2 = (self.Lorentz_force(self._c2, v2, B)[1] - self.Coulomb_force(p1, p2)[1])/self._m2
        az2 = (self.Lorentz_force(self._c2, v2, B)[2] - self.Coulomb_force(p1, p2)[2])/self._m2

        return ax1, ay1, az1, ax2, ay2, az2
        
    def interaction(self, campo_magnetico, total_time, dt=0.1):

        # Aceleración inicial
        ax1, ay1, az1, ax2, ay2, az2 = self.acceleration(self._p1, self._p2, self._v1, self._v2, campo_magnetico)
        
        time=np.arange(0,total_time,dt)

        p1x = [self._p1[0]]
        p1y = [self._p1[1]]
        p1z = [self._p1[2]]

        p2x = [self._p2[0]]
        p2y = [self._p2[1]]
        p2z = [self._p2[2]]

        v1x = [self._v1[0]]
        v1y = [self._v1[1]]
        v1z = [self._v1[2]]

        v2x = [self._v2[0]]
        v2y = [self._v2[1]]
        v2z = [self._v2[2]]

        a1x = [ax1]
        a1y = [ay1]
        a1z = [az1]

        a2x = [ax2]
        a2y = [ay2]
        a2z = [az2]

        for t in time:
            dt = 0.1
            # Posición de la partícula 1
            p1x.append(p1x[-1] + v1x[-1]*dt + 0.5*a1x[-1]*dt**2)
            p1y.append(p1y[-1] + v1y[-1]*dt + 0.5*a1y[-1]*dt**2)
            p1z.append(p1z[-1] + v1z[-1]*dt + 0.5*a1z[-1]*dt**2)

            # Posición de la partícula 2
            p2x.append(p2x[-1] + v2x[-1]*dt + 0.5*a2x[-1]*dt**2)
            p2y.append(p2y[-1] + v2y[-1]*dt + 0.5*a2y[-1]*dt**2)
            p2z.append(p2z[-1] + v2z[-1]*dt + 0.5*a2z[-1]*dt**2)

            # Velocidad de la partícula 1
            v1x.append(v1x[-1] + a1x[-1]*dt)
            v1y.append(v1y[-1] + a1y[-1]*dt)
            v1z.append(v1z[-1] + a1z[-1]*dt)

            # Velocidad de la partícula 2
            v2x.append(v2x[-1] + a2x[-1]*dt)
            v2y.append(v2y[-1] + a2y[-1]*dt)
            v2z.append(v2z[-1] + a2z[-1]*dt)

            ax1, ay1, az1, ax2, ay2, az2 = self.acceleration([p1x[-1], p1y[-1], p1z[-1]], [p2x[-1], p2y[-1], p2z[-1]], [v1x[-1], v1y[-1], v1z[-1]], [v2x[-1], v2y[-1], v2z[-1]], campo_magnetico)

            a1x.append(ax1)
            a1y.append(ay1)
            a1z.append(az1)

            a2x.append(ax2)
            a2y.append(ay2)
            a2z.append(az2)

        p1 = np.array([p1x, p1y, p1z]).T
        p2 = np.array([p2x, p2y, p2z]).T
        v1 = np.array([v1x, v1y, v1z]).T
        v2 = np.array([v2x, v2y, v2z]).T
        a1 = np.array([a1x, a1y, a1z]).T
        a2 = np.array([a2x, a2y, a2z]).T
        print('p1', p1)
        return p1, p2, v1, v2, a1, a2

    def interaction_odeint(self, campo_magnetico, total_time, dt=0.1):
        
        # Sistema de ecuaciones diferenciales
        def system(estado_actual, t, B):
            p1x, v1x, p1y, v1y, p1z, v1z, p2x, v2x, p2y, v2y, p2z, v2z = estado_actual

            a1x, a1y, a1z, a2x, a2y, a2z = self.acceleration([p1x, p1y, p1z], [p2x, p2y, p2z], [v1x, v1y, v1z], [v2x, v2y, v2z], B)

            return [v1x, a1x, v1y, a1y, v1z, a1z, v2x, a2x, v2y, a2y, v2z, a2z]

        # Condiciones iniciales
        initial_state = [self._p1[0], self._v1[0], self._p1[1], self._v1[1], self._p1[2], self._v1[2], self._p2[0], self._v2[0], self._p2[1], self._v2[1], self._p2[2], self._v2[2]]  # Starting at position 0 with velocity 0

        # Tiempo
        time=np.arange(0,total_time,dt)

        # Resolver ODE
        solution = odeint(system, initial_state, time, args=(campo_magnetico,))

        # Soluciones
        p1x = solution[:, 0]
        v1x = solution[:, 1]
        p1y = solution[:, 2]
        v1y = solution[:, 3]
        p1z = solution[:, 4]
        v1z = solution[:, 5]
        p2x = solution[:, 6]
        v2x = solution[:, 7]
        p2y = solution[:, 8]
        v2y = solution[:, 9]
        p2z = solution[:, 10]
        v2z = solution[:, 11]

        p1 = np.array([p1x, p1y, p1z]).T
        p2 = np.array([p2x, p2y, p2z]).T
        v1 = np.array([v1x, v1y, v1z]).T
        v2 = np.array([v2x, v2y, v2z]).T

        a_result = [self.acceleration(p1[i], p2[i], v1[i], v2[i], campo_magnetico) for i in range(len(p1))]
        a1 = np.array([a[0:3] for a in a_result])
        a2 = np.array([a[3:] for a in a_result])

        return p1, p2, v1, v2, a1, a2

    def plot_interaction(self, p1, p2, v1, v2, a1, a2, total_time):

        time = np.linspace(0, total_time, len(p1)-1)

        fig, axs = plt.subplots(3, 6, figsize=(16, 16))

        fig.suptitle('Interacción entre dos partículas')

        p1x = [p[0] for p in p1]
        p1y = [p[1] for p in p1]
        p1z = [p[2] for p in p1]

        axs[0, 0].plot(time, p1x[:-1], 'r')
        axs[0, 0].set_title('Coordenada x')
        axs[0, 0].set_ylabel('p')
        axs[0, 1].plot(time, p1y[:-1], 'r')
        axs[0, 1].set_title('Partícula 1 \n Coordenada y')

        axs[0, 2].plot(time, p1z[:-1], 'r')
        axs[0, 2].set_title('Coordenada z')

        p2x = [p[0] for p in p2]
        p2y = [p[1] for p in p2]
        p2z = [p[2] for p in p2]

        axs[0, 3].plot(time, p2x[:-1], 'b')
        axs[0, 3].set_title('Coordenada x')

        axs[0, 4].plot(time, p2y[:-1], 'b')
        axs[0, 4].set_title('Partícula 2 \n Coordenada y')

        axs[0, 5].plot(time, p2z[:-1], 'b')  
        axs[0, 5].set_title('Coordenada z')

        v1x = [v[0] for v in v1]
        v1y = [v[1] for v in v1]
        v1z = [v[2] for v in v1]

        axs[1, 0].plot(time, v1x[:-1], 'r')
        axs[1, 0].set_ylabel('v')

        axs[1, 1].plot(time, v1y[:-1], 'r')

        axs[1, 2].plot(time, v1z[:-1], 'r')

        v2x = [v[0] for v in v2]
        v2y = [v[1] for v in v2]
        v2z = [v[2] for v in v2]

        axs[1, 3].plot(time, v2x[:-1], 'b')

        axs[1, 4].plot(time, v2y[:-1], 'b')

        axs[1, 5].plot(time, v2z[:-1], 'b')

        
        a1x = [a[0] for a in a1]
        a1y = [a[1] for a in a1]
        a1z = [a[2] for a in a1]

        axs[2, 0].plot(time, a1x[:-1], 'r')
        axs[2, 0].set_xlabel('t')
        axs[2, 0].set_ylabel('a')

        axs[2, 1].plot(time, a1y[:-1], 'r')
        axs[2, 1].set_xlabel('t')

        axs[2, 2].plot(time, a1z[:-1], 'r')
        axs[2, 2].set_xlabel('t')

        a2x = [a[0] for a in a2]
        a2y = [a[1] for a in a2]
        a2z = [a[2] for a in a2]

        axs[2, 3].plot(time, a2x[:-1], 'b')
        axs[2, 3].set_xlabel('t')

        axs[2, 4].plot(time, a2y[:-1], 'b')
        axs[2, 4].set_xlabel('t')

        axs[2, 5].plot(time, a2z[:-1], 'b')
        axs[2, 5].set_xlabel('t')
        
        plt.show()
        plt.savefig('posiciones.png')

    def plot_3D(self, p1, p2):
        
        p1x = [p[0] for p in p1]
        p1y = [p[1] for p in p1]
        p1z = [p[2] for p in p1]

        p2x = [p[0] for p in p2]
        p2y = [p[1] for p in p2]
        p2z = [p[2] for p in p2]

        # Create a new figure
        fig = plt.figure()

        # Add a 3D subplot
        ax = fig.add_subplot(111, projection='3d')

        ax.plot(p1x, p1y, p1z, label='Particle 1')
        ax.plot(p2x, p2y, p2z, label='Particle 2')

        ax.set_xlabel('X Axis')
        ax.set_ylabel('Y Axis')
        ax.set_zlabel('Z Axis')
        ax.legend()

        # Show the plot
        plt.show()
        plt.savefig('posiciones-3D.png')