from simulation import Interaccion, Particulas

if __name__ == '__main__':

    # Instanciamos las particulas
    particula1 = Particulas(1.,1.)
    particula2 = Particulas(-1.,1.)
    
    # Definimos algunos parametros
    time = 20
    frames = 200
    ffmpeg_path = r'C:\PATH_ffmpeg\ffmpeg.exe'

    # Instanciamos la interaccion
    interaccion1 = Interaccion(particula1.c,particula2.c,particula1.m,particula2.m,[1,0, 0],[-1,0,0],[0,1,0],[0,-1,0])
    p1, p2, v1, v2, a1, a2 = interaccion1.interaction([0,0,1],time) # Solucionamos el sistema de ecuaciones
    #p1, p2, v1, v2, a1, a2 = interaccion1.interaction_odeint([0,0,1],time) # Solucionamos el sistema de ecuaciones con odeint

    # Graficamos
    interaccion1.plot_interaction(p1, p2, v1, v2, a1, a2, time) # Graficamos la interaccion
    interaccion1.plot_3D(p1, p2) # Graficamos en 3D
    #interaccion1.plot_3D_animate(p1, p2, frames, ffmpeg_path=ffmpeg_path) # Graficamos en 3D animado